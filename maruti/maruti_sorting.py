import pandas as pd
from pandas import DataFrame, read_csv
import os
# print(os.getcwd())

df = pd.read_csv('maruti_dataset.csv')
df["Price in INR"] = df["Price in INR"].apply(lambda x: x.split('*')[0]).astype('float')
df["Distance(km)"] =  df["Distance(km)"].apply(lambda x: x.split()[0].replace(',','')).astype('float')
df = df.rename(columns= ({'Vehcile Name': 'Vehicle Name'}))
df.to_csv(r'/home/kumar/scraping/maruti/maruti_datasets.csv', index = 0)
