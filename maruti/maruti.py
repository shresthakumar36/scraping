from bs4 import BeautifulSoup
import csv
import dryscrape


csv_file = open('maruti_dataset.csv', 'a')
csv_writer = csv.writer(csv_file)
# csv_writer.writerow(['Vehcile Name','Manufacturing Year','Fuel Type','Distance(km)','Price in INR'])
for i in range(560,):
	try:
		print(i)
		url = 'https://www.marutisuzukitruevalue.com/buy-car/{}'.format(i)
		session = dryscrape.Session()
		session.visit(url)
		response = session.body()
		soup = BeautifulSoup(response, 'lxml')

		main = soup.find('body')
		cars_result = main.find('div', id = 'cars-result')
		for car_detailbox in cars_result.find_all('div',class_ = 'car-detail-box'):
			row = []
			row.append(car_detailbox.p.text) #print car name
			
			for ul in car_detailbox.find_all('ul', class_="spec"):
				for li in ul.find_all('li'):
					row.append(li.text)
			for col_sm_8_price_sec_mrgTop in car_detailbox.find_all('div',class_= 'col-sm-8 price-sec mrgTop'):
				for priceDisplay_prc in col_sm_8_price_sec_mrgTop.find_all('span',class_ = 'priceDisplay'):
					# print(priceDisplay_prc.text)
					row.append(priceDisplay_prc.text)
			csv_writer.writerow(row)
	except:
		continue
csv_file.close()