
from bs4 import BeautifulSoup
import csv
import dryscrape
import pandas as pd
from pandas import DataFrame, read_csv

q = 'global'
csv_file = open('autoportal1.csv', 'a')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['Car Name', 'Distance','Fuel Type', 'Location','Manufacturing Year','Price_in_Lakh','','','','','','','','','',''])
df = pd.read_csv('autoportal.csv')
for i in range(320,321):
	try:
		print(i)
		url = 'https://autoportal.com/usedcars/finder/page-{}/'.format(i)
		session = dryscrape.Session()
		session.visit(url)
		response = session.body()
		soup = BeautifulSoup(response, 'lxml')

		main = soup.find('body')
		# print(main.prettify())
		for cell8_cell_md_pull_right in main.find_all('div',class_ = 'cell8 cell-md pull-right'):
			for item_in in cell8_cell_md_pull_right.find_all('div', class_ = 'item-in'):
				row = [(item_in.a.text.strip())]#print car name
				for desc_desc_info in item_in.find_all('div', class_ = 'desc desc-info'):
					detail = desc_desc_info.text.strip()
					# print(detail)
					if '•' in detail:
						detail_list = detail.split('•')
						for d in detail_list:
							row.append(d) #PRINT DETAILS
					# if detail != '•':
					else:
						row.append(detail)
				for clearfix in item_in.find_all('div', class_ = 'clearfix'):
					prc = clearfix.p.text
					price = str(prc)
					p = []
					p = (price.split(' '))
					q = float(p[1])
					row.append(q)
				csv_writer.writerow(row)
	except:
		continue
csv_file.close()

