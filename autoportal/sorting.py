import pandas as pd
from pandas import DataFrame, read_csv
import os
# print(os.getcwd())
df = pd.read_csv('autoportal.csv')

# print(df.dtypes)

df["Distance"] =  df["Distance"].apply(lambda x: x.split()[0].replace(',','')).astype('float')
df["Price"] = df["Price"].apply(lambda x: x.split()[0].replace(',','')).astype('float')

df["Manufacturing Year"] = df["Manufacturing Year"].apply(lambda x: x.split()[0]).astype('int')



df = df.rename(columns= ({'Distance': 'Distance(km)'}))
df = df.rename(columns= ({'Car Name': 'Vehicle Name'}))
df = df.sort_values(['Vehicle Name'], ascending=True)
df = df.dropna(axis = 'columns')
df['Price'] = df["Price"]*10000
df = df.rename(columns= ({'Price': 'Price in INR'}))
df.to_csv(r'/home/kumar/scraping/autoportal/autoportal_dataset.csv', index = 0)
