# import dryscrape
import requests
from bs4 import BeautifulSoup
import csv
# import grapheme

csv_file = open('car_detials.csv', 'w')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['Vehicle Name','Price', 'Distance Travelled', 'Fuel Type','Previous Owners','Manufacturing Year'])
	# )
for i in range(1,26):
	url = 'https://www.cartrade.com/buy-used-cars/p-' + str(i)


	session = requests.session()
	response = session.get(url)
	soup = BeautifulSoup(response.text, "lxml")

	main = soup.find('body')
	form1 = main.find('form', id = 'filter_search')

	try:
		searchFilters = form1.find('div', id='searchFilters')
		carlistblk = searchFilters.find_all('div', class_ = ['blk_grid_new', 'odd carlistblk_new', 'carlistblk'])
		
		for grid_cnt_new1 in carlistblk:
			grid_cnt_new= grid_cnt_new1.find('div', class_ = 'grid_cnt_new')

			prc_EMI = grid_cnt_new.find('div', class_ = 'price_EMI')
			prc_blk = prc_EMI.find('div', class_  = 'prc_blk')
			cr_prc = prc_blk.find('div' , class_ = 'cr_prc')
			info_car_new = grid_cnt_new.find('div', class_ = 'info_cr_new')
			ul = info_car_new.find('ul')
			row = [grid_cnt_new.h2.a.text.strip(),cr_prc.text.strip()]
			for li in ul.find_all('li'):
				detail = li.text.strip()
				if detail != '|':
					row.append(detail)
				# details = '-'.join([li.text for li in ul.find_all('li')])
				# csv_writer.writerow([ details.strip()])
			csv_writer.writerow(row)
		# 	

	except:
		continue
csv_file.close()
