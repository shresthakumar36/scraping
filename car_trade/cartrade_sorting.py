import pandas as pd
from pandas import DataFrame, read_csv
import os
print(os.getcwd())

df = pd.read_csv('/home/kumar/scraping/car_trade/cartrade.csv')
# print(df.dtypes)
df = df.rename(columns=({'Distance Travelled':'Distance(km)'}))

df["Distance(km)"] =  df["Distance(km)"].apply(lambda x: x.split()[0].replace(',','')).astype('float')
df["Price"] = df["Price"].apply(lambda x: x.split()[1].replace(',','')).astype('float')
df = df.rename(columns= ({'Price': 'Price in INR'}))

# df["Manufacturing Year"] = df["Manufacturing Year"].apply(lambda x: x.split()[0]).astype('float')
df.to_csv(r'/home/kumar/scraping/car_trade/cartrade_dataset.csv', index = 0)
print(df.dtypes)