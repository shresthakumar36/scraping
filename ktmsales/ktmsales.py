from bs4 import BeautifulSoup
import csv
import dryscrape


csv_file = open('ktmcarsales.csv', 'a')
csv_writer = csv.writer(csv_file)
# csv_writer.writerow(['Car Name', 'Price','', '','','','','','','','','','','','',''])

for i in range(76,94):
	try:


		print(i)
		url = 'http://www.ktmcarsales.com/buy-cars-kathmandu-nepal/page/{}/'.format(i)
		session = dryscrape.Session()
		session.visit(url)
		response = session.body()
		soup = BeautifulSoup(response, 'lxml')

		main = soup.find('body')
		# print(main.prettify())
		for content_left in main.find_all('div', class_ = 'content_left'):
			for post_right_full in content_left.find_all('div', class_ = 'post-right full'):
				row = post_right_full.h3.text.strip(), post_right_full.p.text.strip()
				csv_writer.writerow(row)
	except:
		continue
csv_file.close()

