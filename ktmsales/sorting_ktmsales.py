import pandas as pd
from pandas import DataFrame, read_csv
import os
import bigfloat
# print(os.getcwd())

df = pd.read_csv('ktmcarsales.csv')
df = df.dropna(axis = 'columns')
df["Price"] = df["Price"].apply(lambda x: x.split()[1]).astype('bigfloat')
print(df.dtypes)