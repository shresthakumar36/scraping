import requests
from bs4 import BeautifulSoup
import csv
import dryscrape


csv_file = open('carguru_detials.csv', 'a')
csv_writer = csv.writer(csv_file)
# csv_writer.writerow(['Car Name', '','', '','','Price','','','','','Mileage'])

for i in range(60,68):
	try:
		print(i)

		url = 'https://www.cargurus.com/Cars/spt_used_cars#resultsPage={}'.format(i)
		session = dryscrape.Session()
		session.visit(url)
		response = session.body()
		soup = BeautifulSoup(response, 'lxml')

		main = soup.find('body')
		mainSearchResultsContainer = main.find('div' , id = 'mainSearchResultsContainer')
		searchResultsContainer2 = mainSearchResultsContainer.find('div',id ='searchResultsContainer2')
		for each_car_block in searchResultsContainer2.find_all(class_="ft-car cg-dealFinder-result-wrap clearfix"):
			row = []
			row.append(each_car_block.find('h4').text.strip()) #car name

			for p in each_car_block.find_all('p'):
				row.append(p.text.strip()) #
				

			csv_writer.writerow(row)
	except:
		continue
csv_file.close()


# for element in p_elements:
#    if element.find('strong') is not None:
#       p_strong_elements.append(element)